package benchmark;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import concurrent.ConcurrentIDW;
import data.Data;
import serial.SerialIDW;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 4, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 4, time = 1, timeUnit = TimeUnit.SECONDS)
@OutputTimeUnit(TimeUnit.SECONDS)
@Fork(1)
public class IDWMicrobenchmark {
    private static int decay;
    private static int pointsToCalculate;
    private static int nThreads;    
    private static boolean isTest;
    private static int maxCoordinates;
	private static Data data;
    
    @Setup
	public static final void setup() {
		decay = 2;
        pointsToCalculate = 200;
        nThreads = Runtime.getRuntime().availableProcessors();
		isTest = false;
        maxCoordinates = 8000;  
		data = new Data(maxCoordinates, isTest);
	}

	@Benchmark
	public void serialIDW() {
        SerialIDW serialIDW = new SerialIDW();
        serialIDW.calculate(data.getDataArray(), data.getCalculationArray(), decay, pointsToCalculate);
	}

	@Benchmark
	public void concurrentIDW() {
		ConcurrentIDW concurrentIDW = new ConcurrentIDW();
        concurrentIDW.startConcurrent(nThreads, data.getDataArray(), data.getCalculationArray(), pointsToCalculate, decay);
	}
}
