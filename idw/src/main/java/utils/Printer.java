package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Printer {
	private final int emptyCell = -100;
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss");
    private LocalDateTime now;

    public void print2DDoubleArray(double[][] array, int dataSize){
        StringBuilder resultPrint = new StringBuilder();
        resultPrint.append("\n");

        for(int line = 0; line < dataSize; line++){
            for (int column = 0; column < dataSize; column++){
                if(array[line][column] == emptyCell){
                    resultPrint.append("|       |\t");
                }
                else {
                    if(array[line][column] > 10){
                        resultPrint.append("| " + String.format(Locale.ROOT, "%.2f", array[line][column]) + " |\t");
                    } else if (array[line][column] < -10) {
                        resultPrint.append("|" + String.format(Locale.ROOT, "%.2f", array[line][column]) + " |\t");
                    } else if (array[line][column] < 0) {
                        resultPrint.append("| " + String.format(Locale.ROOT, "%.2f", array[line][column]) + " |\t");
                    } else {
                        resultPrint.append("|  " + String.format(Locale.ROOT, "%.2f", array[line][column]) + " |\t");
                    }
                }
            }
            resultPrint.append("\n");
        }

        System.out.println(resultPrint.toString());
    }

    public void readerStart(){
        now = LocalDateTime.now();  
        System.out.println("(" + dateFormat.format(now) + ") Starting to read CSV file...");
    }

    public void readerDone(){
        now = LocalDateTime.now();  
        System.out.println("(" + dateFormat.format(now) + ") Reading done!");
    }

    public void serialStart(){
        now = LocalDateTime.now();  
        System.out.println("(" + dateFormat.format(now) + ") Starting to calculate serial IDW...");
    }

    public void serialDone(){
        now = LocalDateTime.now();  
        System.out.println("(" + dateFormat.format(now) + ") IDW Serial done! ");
    }

    public void concurrentStart(){
        now = LocalDateTime.now();  
        System.out.println("(" + dateFormat.format(now) + ") Starting to calculate concurrent IDW...");
    }

    public void concurrentDone(){
        now = LocalDateTime.now();  
        System.out.println("(" + dateFormat.format(now) + ") IDW concurrent done! ");
    }
}
