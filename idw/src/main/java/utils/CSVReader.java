package utils;

import java.io.BufferedReader;
import java.io.FileReader;
 
public class CSVReader {
	private int value;
	private int x;
	private int y;
	private int emptyCell;

	public CSVReader(){
		value = 0;
		x = 1;
		y = 2;
		emptyCell = -100;
	}

	public double[][] read(int maxCoordinates, String filePath, double[][] data) {
		String line;
		try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
			line = reader.readLine(); // jump header

			while((line = reader.readLine()) != null){
				String[] rowData = line.split(","); // string array format [value][x][y]
				double value_data = Double.parseDouble(rowData[value]);
				int x_data = Integer.parseInt(rowData[x]);
				int y_data = Integer.parseInt(rowData[y]);
				if(data[x_data][y_data] == emptyCell) {
					data[x_data][y_data] = value_data;
				}
				else { // if the coordinate already had a value, atribute average of both values
					data[x_data][y_data] = (data[x_data][y_data] + value_data) / 2;
				}
			}
		} catch (Exception e){
			System.out.println(e);
		}
		return data;
	}
}