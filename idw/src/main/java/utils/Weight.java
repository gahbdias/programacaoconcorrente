package utils;

import java.lang.Math;

public class Weight {
    // calculates de distance between two coordinates
    public double getDistance(int xA, int yA, int xB, int yB){
        return Math.sqrt(Math.pow((yB - yA), 2) + Math.pow((xB - xA), 2));
    }

    // calculates 1/distance^decay
    public double calculate(int xA, int yA, int xB, int yB, int decay){
        double distance = getDistance(xA, yA, xB, yB);

        return 1/Math.pow(distance, decay);
    }
}