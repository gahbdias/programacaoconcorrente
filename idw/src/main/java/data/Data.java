package data;

import utils.CSVReader;

public class Data {
    private int maxCoordinates;
    private int emptyCell;
	private String filePath;
    private double[][] dataArray;
    private double[][] calculationArray;

    private CSVReader csvReader;

    public Data(int size, boolean isTest){
        maxCoordinates = size;
        emptyCell = -100;

        if(isTest){
            filePath = "C:\\Users\\gahbd\\Desktop\\Facul\\GitLab\\programacaoconcorrente\\idw\\datasets\\test.csv";
        } else {
            filePath = "C:\\Users\\gahbd\\Desktop\\Facul\\GitLab\\programacaoconcorrente\\idw\\datasets\\dataset.csv";
        }

        csvReader = new CSVReader();
        initiateDataArray();
        dataArray = csvReader.read(maxCoordinates, filePath, dataArray);
        initiateCalculationArray();
    }

    public void initiateDataArray(){
        dataArray = new double[maxCoordinates][maxCoordinates];
        for(int i=0; i<maxCoordinates; i++){
            for(int j=0; j<maxCoordinates; j++){
                dataArray[i][j] = emptyCell;
            } 
        }        
    }

    public void initiateCalculationArray(){
        calculationArray = new double[maxCoordinates][maxCoordinates];
        for(int i=0; i<maxCoordinates; i++){
            for(int j=0; j<maxCoordinates; j++){
                calculationArray[i][j] = dataArray[i][j];
            } 
        }        
    }

    public double[][] getDataArray() {
        return this.dataArray;
    }

    public void setDataArray(double[][] dataArray) {
        this.dataArray = dataArray;
    }

    public double[][] getCalculationArray() {
        return this.calculationArray;
    }

    public void setCalculationArray(double[][] calculationArray) {
        this.calculationArray = calculationArray;
    }
    
}
