package concurrent;

import java.security.SecureRandom;

import utils.Weight;

public class ConcurrentIDW {
    private Weight weight;
    private SecureRandom random;
    private int emptyCell;

    public ConcurrentIDW(){
        weight = new Weight();
        random = new SecureRandom();
        emptyCell = -100;
    }

    public void startConcurrent(int nThreads, double[][] dataArray, double[][] calculationArray, int pointsToCalculate, int decay) {
        IDWThread[] threads = new IDWThread[nThreads];
        int localLength = dataArray[0].length/nThreads;
        int localPoints = pointsToCalculate/nThreads;
        
        for(int i = 0; i < nThreads; i++){
            int startIndex = i * localLength;
            int endIndex = (i + 1) * localLength;
		    threads[i] = new IDWThread(startIndex, endIndex, dataArray, calculationArray, localPoints, decay, this);
		    threads[i].start();
		}

        for (IDWThread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }
    }

    /*
     *  result = sum from 1 to maxCoordinate of values x 1/distances^decay
     *           divided by sum from 1 to maxCoordinate of 1/distances^decay
    */
    public void calculate(int startIndex, int endIndex, double[][] dataArray, double[][] calculationArray, int decay, int pointsToCalculate){
        final int maxCoordinate = dataArray[0].length;
        int pointX = random.nextInt(startIndex, endIndex);
        int pointY = random.nextInt(startIndex, endIndex); 
        int tries = 0;
        int calculated = 0;
        while(tries <= (maxCoordinate*maxCoordinate) && calculated < pointsToCalculate){
            double dividend = 0.0;
            double divisor = 0.0;
            if(dataArray[pointX][pointY] == emptyCell){
                for(int dataX=0; dataX<maxCoordinate; dataX++){
                    for(int dataY=0; dataY<maxCoordinate; dataY++){
                        if(dataArray[dataX][dataY] != emptyCell){
                            dividend += dataArray[dataX][dataY] * weight.calculate(pointX, pointY, dataX, dataY, decay);
                            divisor += weight.calculate(pointX, pointY, dataX, dataY, decay);
                        }
                    }             
                }
                if(divisor != 0.0){
                    calculationArray[pointX][pointY] = dividend / divisor;
                    calculated++;
                }
                pointX = random.nextInt(startIndex, endIndex);
                pointY = random.nextInt(startIndex, endIndex);
                tries++;
            } else {
                pointX = random.nextInt(startIndex, endIndex);
                pointY = random.nextInt(startIndex, endIndex);
                tries++;
            }
        }
    }

    private class IDWThread extends Thread {
        private int startIndex;
        private int endIndex;
        private double[][] dataArray;
        private double[][] localCalculationArray;
        private int localPoints;
        private int decay;
        private ConcurrentIDW concurrent;

        public IDWThread(int inputStart, int inputEnd, double[][] dataArray, double[][] calculationArray, int inputPoints, int inputDecay, ConcurrentIDW inputConcurrent) {
            this.startIndex = inputStart;
            this.endIndex = inputEnd;
            this.dataArray = dataArray;
            localCalculationArray = calculationArray;
            this.localPoints = inputPoints;
            this.decay = inputDecay;
            this.concurrent = inputConcurrent;
        }

        @Override
		public void run() {
			concurrent.calculate(startIndex, endIndex, dataArray, localCalculationArray, decay, localPoints);
		}
	}
}
