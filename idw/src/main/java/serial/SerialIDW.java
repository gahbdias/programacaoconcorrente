package serial;

import java.security.SecureRandom;

import utils.Weight;

public class SerialIDW {
    private Weight weight;
    private SecureRandom random;
    private double emptyCell;

    public SerialIDW(){
        weight = new Weight();
        random = new SecureRandom();
        emptyCell = -100.00;
    }

    /*
     *  result = sum from 1 to maxCoordinate of values x 1/distances^decay
     *           divided by sum from 1 to maxCoordinate of 1/distances^decay
    */
    public void calculate(double[][] dataArray, double[][] calculationArray, int decay, int pointsToCalculate){
        final int maxCoordinate = dataArray[0].length;
        int pointX = random.nextInt(maxCoordinate);
        int pointY = random.nextInt(maxCoordinate);
        int tries = 0;
        int calculated = 0;
        while(tries <= (maxCoordinate*maxCoordinate) && calculated < pointsToCalculate){
            double dividend = 0.0;
            double divisor = 0.0;
            if(dataArray[pointX][pointY] == emptyCell){
                for(int dataX=0; dataX<maxCoordinate; dataX++){
                    for(int dataY=0; dataY<maxCoordinate; dataY++){
                        if(dataArray[dataX][dataY] != emptyCell){
                            dividend += dataArray[dataX][dataY] * weight.calculate(pointX, pointY, dataX, dataY, decay);
                            divisor += weight.calculate(pointX, pointY, dataX, dataY, decay);
                        }
                    }             
                }
                if(divisor != 0.0){
                    calculationArray[pointX][pointY] = dividend / divisor;
                    calculated++;
                }
                pointX = random.nextInt(maxCoordinate);
                pointY = random.nextInt(maxCoordinate);
                tries++;
            } else {
                pointX = random.nextInt(maxCoordinate);
                pointY = random.nextInt(maxCoordinate);
                tries++;
            }
        }
    }
}
