
package main;
import benchmark.IDWMicrobenchmark;

import org.openjdk.jmh.profile.GCProfiler;
import org.openjdk.jmh.profile.StackProfiler;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class MicrobenchmarkController {

    public static void main(String[] args) throws Exception {
		Options opt = new OptionsBuilder()
				.include(IDWMicrobenchmark.class.getSimpleName())
				.warmupIterations(4)
				.shouldDoGC(true)
				.measurementIterations(4)
				.forks(1)
				.addProfiler(GCProfiler.class)
				.addProfiler(StackProfiler.class)
				.jvmArgs("-server", "-Xms2g","-Xmx8g")
                .build();
		try {
			new Runner(opt).run();
		} catch (RunnerException e) {
			e.printStackTrace();
		}
	}
}
