package main;

import serial.SerialIDW;
import utils.Printer;
import concurrent.ConcurrentIDW;
import data.Data;

public class IDWController {
    private int decay;
    private int pointsToCalculate;

    private int nThreads;
    
    private boolean isTest;
    private int maxCoordinates;    

    Printer printer;
    SerialIDW serialIDW;
    ConcurrentIDW concurrentIDW;
    Data data;

    public IDWController(){
        decay = 2;
        pointsToCalculate = 200;
        printer = new Printer();
        serialIDW = new SerialIDW();
        concurrentIDW = new ConcurrentIDW();
        nThreads = Runtime.getRuntime().availableProcessors();
    }

    public void test() {
        isTest = true;
        maxCoordinates = 8;

        printer.readerStart();
        data = new Data(maxCoordinates, isTest);
        printer.readerDone();
        
        printer.serialStart();
        serialIDW.calculate(data.getDataArray(), data.getCalculationArray(), decay, pointsToCalculate);
        printer.print2DDoubleArray(data.getCalculationArray(), maxCoordinates);
        printer.serialDone();

        printer.concurrentStart();
        concurrentIDW.startConcurrent(nThreads, data.getDataArray(), data.getCalculationArray(), pointsToCalculate, decay);
        printer.print2DDoubleArray(data.getCalculationArray(), maxCoordinates);
        printer.concurrentDone();
    }

    public void start() {
        isTest = false;
        maxCoordinates = 8000;      

        printer.readerStart();
        data = new Data(maxCoordinates, isTest);
        printer.readerDone();

        printer.serialStart();
        serialIDW.calculate(data.getDataArray(), data.getCalculationArray(), decay, pointsToCalculate);
        printer.serialDone();

        printer.concurrentStart();
        concurrentIDW.startConcurrent(nThreads, data.getDataArray(), data.getCalculationArray(), pointsToCalculate, decay);
        printer.concurrentDone();
    }

}